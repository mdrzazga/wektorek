package my.njpo;

import java.util.Iterator;

/**
 * Obiekt o cechach podobnych do n-elemetowego wektora nad skalrem S.
 * <p> Przykłady użycia patrz {@link WektorekFactory}
 * @see WektorekFactory
 * @author mdrzazga
 * @param <S>  określa klasę pełniąca role skalara dla wektorka
 */
public interface Wektorek<S> extends Iterable<S>{

    /**
     * Zwraca nowy wektorek będący wynikiem dodania wektorka w. Jezeli w jest innego wymiaru wyrzuca UnsupportedOperationException
     * @param w dodawany wektor
     * @return  wektor sumy 
     */
    Wektorek<S> dodaj(Wektorek<S> w);
    
    /**
     *Zwraca nowy wektorek będący wynikiem mnożenia wektorka przez skalar s. 
     * @param s skalar
     * @return wektor ktorego skladowe są iloczynem oryginalengo wektora i s
     */
    Wektorek<S> pomnoz(S s);
    
    /**
     * Iloczyn skalarny z wektorkiem w. Jezeli w jest innego wymiaru wyrzuca UnsupportedOperationException
     * @param w mnożony wektorek
     * @return wartosc iloczynu skalarnego
     */
    S iloczynSkalarny(Wektorek<S> w);
    
    /**
     * Zwraca nowy wektorek z ustawianą na indeksie i wartoscia s. Pozostałe pozycjami nie sa zmieniane. Indeks liczony jest od zera.
     * @param i 
     * @param s
     * @return wektorek z ustawiona wartoscia s 
     */
    Wektorek<S> zmien(int i, S s);
    
    /**
     * Zwraca wartość z pozycji o indekcie i
     * @param i indeks liczony od zera
     * @return wartość na pozycji i
     */
    S get(int i);
    
    /**
     * Zwraca długość wektorka przy przyjętej w S metryce, najcześciej jest to długość Euklidesowa
     * @return dlugosc wektorka
     */
    double dlugosc();
     
    /**
     * Zwraca wymiar wektorka
     * @return wymiar
     */
    int getWymiar();
    
    /**
     * Zwraca iterator po pozycjach wektorka
     * @return iterator zgodnie z kodem:
     * <pre>
     * {@code
     *      return new Iterator<S>() {
      final int size = getWymiar();
      int i=0;
      public boolean hasNext() { return (i < size); }
      public S next() { return get(i++);}
   };
 }
     * </pre>
     */
    @Override
    default Iterator<S> iterator(){
        return new Iterator<S>() {
            final int size = getWymiar();
            int i=0;
            @Override
            public boolean hasNext() { return (i<size); }

            @Override
            public S next() { return get(i++);}
        };
        
    }
    
}
