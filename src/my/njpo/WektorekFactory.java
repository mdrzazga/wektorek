package my.njpo;

/**
 * Fabryka wektorków nad skalarem S.
 * 
 * <p>
 * <pre>
 * {@code 
 *      WektorekFactory<Long> f = WektorekLongFactory.instance();
 *      WektorekFactory<Long> f2=f.wymiar(2);
 *      WektorekFactory<Long> f3=f2.wymiar(3);
 *      
 *      Wektorek<Long> w1 = f3.jedynka();
 *      Wektorek<Long> w2 = f3.get(1l,2l,3l);
 *      
 *      w2=w2.dodaj(w1);
 *      
 *      Wektorek<Long> w3 = f2.jedynka().pomnorz(3l);
 * }
 * </pre>
 * @author mdrzazga
 * @param <S> określa klasę pełniąca role skalara dla wektorka
 * @see my.njpo.Wektorek
 */
public interface WektorekFactory<S> {

    /**
     * Zwraca wektorek, w którym wszystkie składowe są zerami
     * @return wektorek zerowy
     */
    Wektorek<S> zero();
    
    /**
     * Zwraca wektorek, w którym wszystkie składowe są 1 w S
     * @return wektorek jedynkowy
     */
    Wektorek<S> jedynka();
    
    /**
     * Zwraca wektorek z zadanymi składowymi. Jeżeli  {@code s.leanght() < wymiar() pozostałe skłądowe powinny zostac obsadzone zerami } 
     * @param s tablica skalarow
     * @return  wektorek wypełniony wartosiami
     */
    Wektorek<S> get(S ... s);
    
    /**
     * Zwraca fabrykę wektorów o zadanym wymiarze
     * @param wymiar zadany wymiar
     * @return  fabryka wektorków o zadanym wymiarze
     */
    WektorekFactory<S> wymiar(int wymiar);
    
    /**
     * Zwraca wymiar fabryki
     * @return  wymiar fabryki
     */
    int getWymiar();
    
}
